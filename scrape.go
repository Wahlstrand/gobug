package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

type WorkOrder struct {
	path string
	depth int
}
type WorkResult struct {
	workOrder WorkOrder
	paths []string
}
func check(workOrder WorkOrder) WorkResult{
	//Pretend that this takes some time
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
	return WorkResult{
		workOrder: workOrder,
		paths: []string{
			workOrder.path + "/a",
			workOrder.path + "/b",
			workOrder.path + "/c"}}
}

type Worker struct {
	workOrderChannel <-chan WorkOrder
	workResultChannel chan<- WorkResult
	cancelChannel <-chan struct{}
	wg *sync.WaitGroup
}
func work(this *Worker) {
	defer this.wg.Done()
	for {
		select {
		case workOrder := <-this.workOrderChannel:
			fmt.Printf("worker working on <%s>\n", workOrder.path)
			this.workResultChannel <- check(workOrder)
		case <-this.cancelChannel:
			return
		}
	}
}

type Crawler struct {
	workOrderChannel chan<- WorkOrder
	workResultChannel <-chan WorkResult
	numPendingJobs int
	workQueue []WorkOrder
}
func blockingPush(this *Crawler, workOrder WorkOrder) {
	this.workOrderChannel <- workOrder
	this.numPendingJobs++
}
func nonBlockingPush(this *Crawler, workOrder WorkOrder) bool {
	select {
	case this.workOrderChannel <- workOrder:
		this.numPendingJobs++
		return true
	default:
		return false
	}
}
func schedule(this *Crawler) {
	//Push orders to channel until it is filled, remaining have to wait
	for len(this.workQueue) != 0 && nonBlockingPush(this, this.workQueue[0]) {
		this.workQueue = this.workQueue[1:]
	}
}
func complete(this *Crawler, workResult WorkResult, maxDepth int) {
	this.numPendingJobs--
	//Create work orders for any sub-paths found if depth is not too great
	if workResult.workOrder.depth < maxDepth {
		for _, path := range workResult.paths {
			this.workQueue = append(this.workQueue, WorkOrder{path: path, depth: workResult.workOrder.depth + 1})
		}
	}
}
func isDone(this *Crawler) bool {
	return this.numPendingJobs == 0
}
func awaitResult(this *Crawler) WorkResult {
	return <-this.workResultChannel
}
func crawl(this *Crawler, root string, maxDepth int) {
	//Push the initial work order. This must be blocking in case
	//the worker threads are not listening yet.
	blockingPush(this, WorkOrder{path: root, depth: 0})
	for !isDone(this) {
		//Await a result
		workResult := awaitResult(this)

		//process these results and queue new work orders as required
		//(added to this.workQueue)
		complete(this, workResult, maxDepth)

		//Push as many jobs as possible to free worker threads
		schedule(this)
	}
}

func run(root string, maxDepth int, maxThreads int) {
	//Synchronization resources
	workResultChannel := make(chan WorkResult)
	workOrderChannel := make(chan WorkOrder)
	cancelChannel := make(chan struct{})
	wg := sync.WaitGroup{}

	//These workers do the actual scraping of single pages
	for n := 0; n < maxThreads; n++ {
		wg.Add(1)
		go work(&Worker{workOrderChannel, workResultChannel, cancelChannel, &wg})
	}

	//The crawler receives results from the workers and pushes new work orders
	//onto the workOrderChannel
	crawl(&Crawler{workOrderChannel: workOrderChannel, workResultChannel: workResultChannel}, root, maxDepth)

	//Kill workers
	close(cancelChannel)
	close(workResultChannel)
	wg.Wait()

	//Close remaining channels
	close(workOrderChannel)
}

func main() {
	run("root.net", 3, 4)
}
